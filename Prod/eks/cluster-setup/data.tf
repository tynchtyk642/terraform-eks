data "terraform_remote_state" "eks" {
  backend = "local"

  config = {
    path = "${path.module}/../cluster-provisioning/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "local"

  config = {
    path = "${path.module}/../../vpc/terraform.tfstate"
  }
}
