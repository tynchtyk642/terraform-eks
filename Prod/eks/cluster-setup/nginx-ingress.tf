# Load balancer Controller
resource "kubernetes_service_account" "lb_controller" {
  metadata {
    name      = "aws-load-balancer-controller"
    namespace = "kube-system"
    labels = {
      "app.kubernetes.io/component" = "controller"
      "app.kubernetes.io/name"      = "aws-load-balancer-controller"
    }
    annotations = {
      "eks.amazonaws.com/role-arn" = data.terraform_remote_state.eks.outputs.elb_controller_role_arn
    }
  }
}

resource "helm_release" "lb_controller" {
  chart      = "aws-load-balancer-controller"
  name       = "lb-controller"
  namespace  = "kube-system"
  repository = "https://aws.github.io/eks-charts"

  set {
    name  = "clusterName"
    value = data.terraform_remote_state.eks.outputs.cluster_name
  }

  set {
    name  = "serviceAccount.create"
    value = false
  }

  set {
    name  = "serviceAccount.name"
    value = "aws-load-balancer-controller"
  }

  depends_on = [kubernetes_service_account.lb_controller]
}

# nginx-ingress
resource "helm_release" "nginx-ingress" {
  name      = "nginx-ngress"
  namespace = "nginx-ingress"
  create_namespace = true
  repository = "https://helm.nginx.com/stable"
  chart      = "nginx-ingress"

  values = [file("${path.module}/Charts/nginx-ingress/values.yml")]

  depends_on = [helm_release.lb_controller]
}

resource "kubectl_manifest" "certClusterIssuer" {
  yaml_body = templatefile("${path.module}/Charts/nginx-ingress/cert-clusterIssuer.yml.tpl", {
    email = "tynchtyk642@gmail.com"
    region = var.region
    hosted_zone_id = ""
    cert_manager_role_arn = data.terraform_remote_state.eks.outputs.cert_role_arn
    domain_name = ""
  })

  depends_on = [helm_release.nginx-ingress]
}

resource "kubectl_manifest" "nginx-certificate" {
  yaml_body = templatefile("${path.module}/Charts/nginx-ingress/certificate.yml.tpl", {
    application_domain_name = ""
  })

  depends_on = [helm_release.nginx-ingress]
}

resource "kubectl_manifest" "nginx-virtualserver" {
  yaml_body = templatefile("${path.module}/Charts/nginx-ingress/nginx-virtualserver.yml.tpl", {
    application_domain_name = ""
  })

  depends_on = [helm_release.nginx-ingress]
}