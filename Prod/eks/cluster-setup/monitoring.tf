locals {
  monitoring_namespace = "monitoring"
}

resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = local.monitoring_namespace
  }
}

resource "kubernetes_service_account" "example" {
  metadata {
    name      = "aws-load-balancer-controller"
    namespace = "kube-system"
    labels = {
      "app.kubernetes.io/component" = "controller"
      "app.kubernetes.io/name"      = "aws-load-balancer-controller"
    }
    annotations = {
      "eks.amazonaws.com/role-arn" = data.terraform_remote_state.eks.outputs.elb_controller_role_arn
    }
  }
}

resource "kubernetes_storage_class" "prometheus" {
  metadata {
    name = "prometheus"
    annotations = {
      namespace = "monitoring"
    }
  }
  storage_provisioner = "kubernetes.io/aws-ebs"
  reclaim_policy      = "Retain"
  volume_binding_mode = "Immediate"
  parameters = {
    type = "gp2"
  }
}

# Load balancer Controller
resource "helm_release" "lb_controller" {
  chart      = "aws-load-balancer-controller"
  name       = "aws-load-balancer-controller"
  namespace  = "kube-system"
  repository = "https://aws.github.io/eks-charts"

  set {
    name  = "clusterName"
    value = data.terraform_remote_state.eks.outputs.cluster_name
  }

  set {
    name  = "serviceAccount.create"
    value = false
  }

  set {
    name  = "serviceAccount.name"
    value = "aws-load-balancer-controller"
  }
}



# Monitoring applications
module "monitoring" {
  source = "github.com/andreswebs/terraform-aws-eks-monitoring"

  cluster_oidc_provider = data.terraform_remote_state.eks.outputs.oidc_provider

  loki_iam_role_name           = "loki-${data.terraform_remote_state.eks.outputs.cluster_id}"
  loki_compactor_iam_role_name = "loki-compactor-${data.terraform_remote_state.eks.outputs.cluster_id}"
  grafana_iam_role_name        = "grafana-${data.terraform_remote_state.eks.outputs.cluster_id}"

  loki_storage_s3_bucket_name = data.terraform_remote_state.vpc.outputs.s3_bucket_id

  grafana_enabled = true
}