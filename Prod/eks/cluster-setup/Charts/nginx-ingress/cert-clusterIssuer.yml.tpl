apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-cluster-issuer
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    email: ${email}
    privateKeySecretRef:
      name: letsencrypt-cluster-issuer-key
    solvers:
    - dns01:
        route53:
          region: ${region}
          hostedZoneID: ${hosted_zone_id}
          role: ${cert_manager_role_arn}
      selector:
        dnsZones:
          - "${domain_name}"