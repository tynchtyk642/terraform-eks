resource "helm_release" "external-dns" {
  name             = "external-dns"
  namespace        = "external-dns"
  create_namespace = true
  repository       = "https://charts.bitnami.com/bitnami"
  chart            = "external-dns"
  version          = "6.7.3"

  set {
    name  = "txtOwnerId"
    value = data.terraform_remote_state.eks.outputs.cluster_name
  }

  set {
    name  = "txtPrefix"
    value = "${data.terraform_remote_state.eks.outputs.cluster_name}."
  }
}