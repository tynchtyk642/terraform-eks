module "grafana_assume_role_policy" {
  source                = "andreswebs/eks-irsa-policy-document/aws"
  version               = "1.0.0"
  cluster_oidc_provider = data.terraform_remote_state.eks.outputs.oidc_provider
  k8s_sa_name           = "grafana"
  k8s_sa_namespace      = "monitoring"
}

resource "aws_iam_role" "grafana" {
  name                  = "grafana"
  assume_role_policy    = module.grafana_assume_role_policy.json
  force_detach_policies = true
}

resource "aws_iam_role_policy" "grafana_permissions" {
  name   = "grafana-permissions"
  role   = aws_iam_role.grafana.id
  policy = file("${path.module}/policies/grafana-permissions.json")
}

resource "helm_release" "grafana" {
  name       = "grafana"
  repository = "https://grafana.github.io/helm-charts"
  chart      = "grafana"
  namespace  = "monitoring"

  recreate_pods    = true
  replace          = true
  create_namespace = true

  values = [
    templatefile("${path.module}/Charts/monitoring/grafana.yml.tpl", {
      aws_region                   = var.region
      prom_svc                     = "prometheus-server.monitoring.svc.cluster.local"
      loki_svc                     = "loki-distributed-gateway.monitoring.svc.cluster.local"
      grafana_service_account_name = "grafana"
      grafana_iam_role_arn         = aws_iam_role.grafana.arn
    })
  ]
}