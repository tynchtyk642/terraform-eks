{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "${node_role_arn}"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}