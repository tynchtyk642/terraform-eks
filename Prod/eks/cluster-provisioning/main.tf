locals {
  env          = "prod"
  cluster_name = "${local.env}-eks-cluster"
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.0.4"

  cluster_name = local.cluster_name

  vpc_id     = data.terraform_remote_state.vpc.outputs.vpc_id
  subnet_ids = data.terraform_remote_state.vpc.outputs.private_subnets
  cluster_endpoint_public_access = true


  eks_managed_node_groups = {
    prod = {
      min_size     = 1
      max_size     = 3
      desired_size = 2

      instance_types = ["t3.medium"]
      labels = {
        Environment = local.env
      }

      tags = {
        Name = "${local.env}-stepstone"
      }
    }
  }

  node_security_group_enable_recommended_rules  = true
  iam_role_name = "aws-eks-tynchtyk-${local.env}"
  cluster_addons = {
    coredns = {
      resolve_conflicts = "OVERWRITE"
    }
    kube-proxy = {}
    vpc-cni = {
      resolve_conflicts = "OVERWRITE"
    }
  }
}
