#  Terraform configuration files to create EKS Infrastructure

1. Deploy vpc folder to create VPC Resources
2. Deploy eks/eks-provisioning to create EKS. After provisioning EKS Cluster run this command inside the EKS in order to using Persistent storage on EKS Cluster:
```bash
aws eks --region <eks_region> update-kubeconfig --name <eks_name>
```
``` bash
kubectl apply -k "github.com/kubernetes-sigs/aws-ebs-csi-driver/deploy/kubernetes/overlays/stable/?ref=master"
```
```bash
kubectl annotate serviceaccount ebs-csi-controller-sa \
  -n kube-system \
  eks.amazonaws.com/role-arn=arn:aws:iam::<YOUR_AWS_ACCOUNT_ID>:role/AmazonEKS_EBS_CSI_DriverRole
```
```bash
kubectl delete pods \
  -n kube-system \
  -l=app=ebs-csi-controller
```
3. Deploy eks/eks-setup to create applications