terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.73"
    }
  }

  #   backend "s3" {
  #     bucket = "<tfstate_bucket>"
  #     key    = "path/to/terraform.tfstate"
  #     region = "eu-west-1"
  #   }
}
