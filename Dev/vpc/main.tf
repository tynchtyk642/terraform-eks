locals {
  env                     = "dev"
  region                  = "us-east-1"
  s3_bucket_name          = "${local.env}-vpc-flow-logs-${random_pet.this.id}"
  destination_bucket_name = ""
  cluster_name            = "${local.env}-eks-cluster"
}

resource "random_pet" "this" {
  length = 3
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.18.1"

  name               = "${local.env}-vpc"
  cidr               = "10.101.0.0/16"
  enable_nat_gateway = true
  single_nat_gateway = true

  azs                 = ["${local.region}a", "${local.region}b", "${local.region}c"]
  public_subnets      = ["10.101.1.0/24", "10.101.2.0/24", "10.101.3.0/24"]
  private_subnets     = ["10.101.11.0/24", "10.101.12.0/24", "10.101.13.0/24"]
  database_subnets    = ["10.101.21.0/24", "10.101.22.0/24", "10.101.23.0/24"]
  elasticache_subnets = ["10.101.31.0/24", "10.101.32.0/24", "10.101.33.0/24"]
  intra_subnets       = ["10.101.41.0/24", "10.101.42.0/24", "10.101.43.0/24"]

  public_subnet_names      = ["${local.env}-public-subnet-1", "${local.env}-public-subnet-2", "${local.env}-public-subnet-3"]
  private_subnet_names     = ["${local.env}-private-subnet-1", "${local.env}-private-subnet-2", "${local.env}-private-subnet-3"]
  database_subnet_names    = ["${local.env}-database-subnet-1", "${local.env}-database-subnet-2", "${local.env}-database-subnet-3"]
  elasticache_subnet_names = ["${local.env}-elasticache-subnet-1", "${local.env}-elasticache-subnet-2", "${local.env}-elasticache-subnet-3"]
  intra_subnet_names       = ["${local.env}-intra-subnet-1", "${local.env}-intra-subnet-2", "${local.env}-intra-subnet-3"]

  public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = 1
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = 1
  }

  public_inbound_acl_rules       = concat(local.network_acls["default_inbound"], local.network_acls["public_inbound"])
  public_outbound_acl_rules      = concat(local.network_acls["default_outbound"], local.network_acls["public_outbound"])
  elasticache_outbound_acl_rules = concat(local.network_acls["default_outbound"], local.network_acls["elasticache_outbound"])

  private_dedicated_network_acl     = false
  elasticache_dedicated_network_acl = true

  manage_default_network_acl = true

  create_database_subnet_group = true


  enable_flow_log           = true
  flow_log_destination_type = "s3"
  flow_log_destination_arn  = module.s3_bucket.s3_bucket_arn
  tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
  }

}


module "s3_bucket" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "~> 3.0"

  bucket                           = local.s3_bucket_name
  policy                           = data.aws_iam_policy_document.flow_log_s3.json
  attach_require_latest_tls_policy = true
  force_destroy                    = true

  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        sse_algorithm = "AES256"
      }
      bucket_key_enabled = true
    }
  }

  #   replication_configuration = {
  #     role = aws_iam_role.replication.arn

  #     rules = [
  #       {
  #         id     = "everything-without-filters"
  #         status = "Enabled"

  #         delete_marker_replication = true

  #         destination = {
  #           bucket        = "arn:aws:s3:::${local.destination_bucket_name}"
  #           storage_class = "STANDARD"
  #         }
  #       },
  #     ]
  #   }



  intelligent_tiering = {
    general = {
      status = "Enabled"
      filter = {
        prefix = "/"
        tags = {
          Environment = local.env
        }
      }
      tiering = {
        DEEP_ARCHIVE_ACCESS = {
          days = 180
        }
      }
    },
  }


  versioning = {
    status = true
    # mfa_delete = true
  }

}

data "aws_iam_policy_document" "flow_log_s3" {
  statement {
    sid = "AWSLogDeliveryWrite"

    principals {
      type        = "Service"
      identifiers = ["delivery.logs.amazonaws.com"]
    }

    actions = ["s3:PutObject"]

    resources = ["arn:aws:s3:::${local.s3_bucket_name}/AWSLogs/*"]
  }

  statement {
    sid = "AWSLogDeliveryAclCheck"

    principals {
      type        = "Service"
      identifiers = ["delivery.logs.amazonaws.com"]
    }

    actions = ["s3:GetBucketAcl"]

    resources = ["arn:aws:s3:::${local.s3_bucket_name}"]
  }
}
