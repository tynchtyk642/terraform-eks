resource "aws_iam_role" "AmazonEKS_EBS_CSI_DriverRole" {
  name               = "AmazonEKS_EBS_CSI_DriverRole"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithWebIdentity"]
    principals {
      type        = "Federated"
      identifiers = [module.eks.oidc_provider_arn]
    }
  }
}


resource "aws_iam_policy" "policy" {
  name   = "AmazonEKS_EBS_CSI_DriverPolicy"
  policy = file("${path.module}/iam/example-iam-policy.json")
}


resource "aws_iam_role_policy_attachment" "attachment" {
  policy_arn = aws_iam_policy.policy.arn
  role       = aws_iam_role.AmazonEKS_EBS_CSI_DriverRole.name
}




# ELB Controller Policy

resource "aws_iam_role" "AmazonEKSLoadBalancerControllerRole" {
  name = "AmazonEKSLoadBalancerControllerRole"
  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "Federated" : "${module.eks.oidc_provider_arn}"
        },
        "Action" : "sts:AssumeRoleWithWebIdentity",
        "Condition" : {
          "StringEquals" : {
            "${substr(module.eks.cluster_oidc_issuer_url, 44, 75)}:aud" : "sts.amazonaws.com",
            "${substr(module.eks.cluster_oidc_issuer_url, 44, 75)}:sub" : "system:serviceaccount:kube-system:aws-load-balancer-controller"
          }
        }
      }
    ]
  })
}


resource "aws_iam_policy" "AWSLoadBalancerControllerIAMPolicy" {
  name   = "AWSLoadBalancerControllerIAMPolicy"
  policy = file("${path.module}/iam/iam_policy.json")
}

resource "aws_iam_role_policy_attachment" "elb_attachment" {
  policy_arn = aws_iam_policy.AWSLoadBalancerControllerIAMPolicy.arn
  role       = aws_iam_role.AmazonEKSLoadBalancerControllerRole.name
}


# Certificate
# data "aws_iam_role" "node_groups_role" {
#   name = local.node_groups_role_name

#   depends_on = [module.eks]
# }

resource "aws_iam_policy" "PolicyForCertManager" {
  name   = "PolicyForCertManager"
  policy = templatefile("${path.module}/iam/cert-policy.json.tpl", {
    zone_id = ""
  })
}

resource "aws_iam_role" "RoleForCertManager" {
  name               = "RoleForCertManager"
  assume_role_policy = templatefile("${path.module}/iam/cert-assume-role.json.tpl", {
    node_role_arn = module.eks.eks_managed_node_groups["dev"].iam_role_arn
  })
}

resource "aws_iam_role_policy_attachment" "cert_attachment" {
  policy_arn = aws_iam_policy.PolicyForCertManager.arn
  role       = aws_iam_role.RoleForCertManager.name
}