# resource "kubernetes_namespace" "app1" {
#   metadata {
#     name = "app1"
#   }
# }

# resource "aws_ebs_volume" "mysql" {
#   availability_zone = "us-east-1a"
#   size              = 10

#   tags = {
#     name = "aws-ebs"
#   }
# }

# resource "kubernetes_storage_class" "example" {
#   metadata {
#     name = "aws-ebs"
#     annotations = {
#       namespace = "app1"
#     }
#   }
#   storage_provisioner = "kubernetes.io/aws-ebs"
#   reclaim_policy      = "Retain"
#   volume_binding_mode = "Immediate"
#   parameters = {
#     type = "gp2"
#   }
# }

# resource "kubernetes_persistent_volume" "mysql" {
#   metadata {
#     name = "aws-ebs"
#     annotations = {
#       "namespace" = "app1"
#     }
#   }
#   spec {
#     capacity = {
#       storage = "5Gi"
#     }
#     access_modes = ["ReadWriteOnce"]
#     persistent_volume_source {
#       aws_elastic_block_store {
#         fs_type   = "ext4"
#         volume_id = aws_ebs_volume.mysql.id
#       }
#     }
#   }

#   depends_on = [
#     kubernetes_namespace.app1
#   ]
# }

# resource "helm_release" "mysql" {
#   name       = "mysql"
#   chart      = "mysql"
#   namespace  = "app1"
#   repository = "https://charts.helm.sh/stable"

#   set {
#     name  = "mysqlRootPassword"
#     value = "mysql"
#   }

#   set {
#     name  = "mysqlUser"
#     value = "mysql"
#   }

#   set {
#     name  = "mysqlPassword"
#     value = "mysql"
#   }

#   set {
#     name  = "mysqlDatabase"
#     value = "mysql"
#   }

#   depends_on = [
#     kubernetes_namespace.app1
#   ]
# }
