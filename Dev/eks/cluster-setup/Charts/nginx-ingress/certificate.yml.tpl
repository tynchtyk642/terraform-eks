apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: example-app
  namespace: nginx-ingress
spec:
  dnsNames:
    - ${application_domain_name}
  secretName: example-app-tls
  issuerRef:
    name: letsencrypt-cluster-issuer
    kind: ClusterIssuer