resource "helm_release" "ambassador" {
  chart      = "edge-stack"
  name       = "edge-stack"
  namespace  = "ambassador"
  repository = "https://getambassador.io"
  values = [file("Charts/ambassador/values.yml")]

  depends_on = [kubernetes_service_account.lb_controller, helm_release.lb_controller]
}


resource "kubectl_manifest" "aes_crds" {
  yaml_body = data.http.crds.body
}

data "http" "crds" {
  url = "https://app.getambassador.io/yaml/edge-stack/3.4.1/aes-crds.yaml"
}