# data "aws_iam_policy_document" "bucket_crud" {
#   statement {
#     sid = "AllowListObjects"
#     actions = [
#       "s3:ListBucket"
#     ]
#     resources = [
#       data.terraform_remote_state.vpc.outputs.s3_bucket_arn
#     ]
#   }

#   statement {
#     sid = "AllowObjectsCRUD"
#     actions = [
#       "s3:DeleteObject",
#       "s3:GetObject",
#       "s3:PutObject"
#     ]
#     resources = [
#       "${data.terraform_remote_state.vpc.outputs.s3_bucket_arn}/*"
#     ]
#   }

# }

# data "aws_iam_policy_document" "loki_permissions" {
#   source_policy_documents = [data.aws_iam_policy_document.bucket_crud.json]
# }

# ## end policy documents

# ## loki role

# module "loki_assume_role_policy" {
#   source                = "andreswebs/eks-irsa-policy-document/aws"
#   version               = "1.0.0"
#   cluster_oidc_provider = data.terraform_remote_state.eks.outputs.oidc_provider
#   k8s_sa_name           = "loki"
#   k8s_sa_namespace      = var.monitoring
# }

# resource "aws_iam_role" "loki" {
#   name                  = "loki"
#   assume_role_policy    = module.loki_assume_role_policy.json
#   force_detach_policies = true
# }

# resource "aws_iam_role_policy" "loki_permissions" {
#   name   = "loki-permissions"
#   role   = aws_iam_role.loki.id
#   policy = data.aws_iam_policy_document.loki_permissions.json
# }

# ## end loki role

# ## loki compactor role

# module "loki_compactor_assume_role_policy" {
#   source                = "andreswebs/eks-irsa-policy-document/aws"
#   version               = "1.0.0"
#   cluster_oidc_provider = data.terraform_remote_state.eks.outputs.oidc_provider
#   k8s_sa_name           = "loki-compactor"
#   k8s_sa_namespace      = var.monitoring
# }

# resource "aws_iam_role" "loki_compactor" {
#   name                  = "loki-compactor"
#   assume_role_policy    = module.loki_compactor_assume_role_policy.json
#   force_detach_policies = true
# }

# resource "aws_iam_role_policy" "loki_compactor_permissions" {
#   name   = "loki-compactor-permissions"
#   role   = aws_iam_role.loki_compactor.id
#   policy = data.aws_iam_policy_document.loki_permissions.json
# }


# resource "helm_release" "loki" {
#   name       = "loki-stack"
#   repository = "https://grafana.github.io/helm-charts"
#   chart      = "loki-stack"
#   namespace  = "monitoring"

#   recreate_pods    = true
#   create_namespace = true

#   values = [
#     file("${path.module}/Charts/monitoring/loki-stack.yml")
#   ]
# }